# H2O Learning Playground

These are some configs for experiments I'm doing to evaluate the [H2O webserver](https://h2o.examp1e.net).

## Interests

- HTTP/2 Server Push: sending many push promises
- Deployment: graceful reloading with many domains
- Scripting: mruby syntax, APIs, gems

## Noted Discoveries

- [h2o/h2o#1114](https://github.com/h2o/h2o/issues/1114) file.index
- [h2o/h2o#1115](https://github.com/h2o/h2o/issues/1115) PUSH_PROMISE and http2-max-concurrent-requests-per-connection
